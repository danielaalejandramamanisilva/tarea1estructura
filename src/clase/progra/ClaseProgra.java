/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase.progra;
import javax.swing.JOptionPane;
public class ClaseProgra {
    public static void main(String[] args) {
        String inputtext=JOptionPane.showInputDialog("introduzca un numero");
        int number=Integer.parseInt(inputtext);
        String result=decimal(number);
        JOptionPane.showMessageDialog(null, "el numero: "+number+" en binario es: "+result);
    }
    public static String decimal(int a){
        String binary="";
        String dig;
        for(int i=a;i>0;i/=2 ){
          if(i%2==1){
              dig="1";
          }  
          else{
              dig="0";
          }
          binary=dig+binary;
        }
        return binary;
    }
}
