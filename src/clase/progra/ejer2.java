/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase.progra;
import javax.swing.JOptionPane;
/**
 *
 * @author Martin Orsi Mamani
 */
public class ejer2 {
    public static void main(String[]args){
        int number=0;
        String input = JOptionPane.showInputDialog("ingrese un numero");
        number=countnumber( Integer.parseInt(input));
        JOptionPane.showMessageDialog(null,"el numero de cifras que tiene el numero: "+input+" es:"+ number);
    }
    public static int countnumber(int number){
        int count=0;
        for(int i=number;i>0;i/=10){
            count++;
        }
        return count;
    }
}
