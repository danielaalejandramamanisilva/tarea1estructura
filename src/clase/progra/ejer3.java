/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase.progra;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;
/**
 *
 * @author Martin Orsi Mamani
 */
public class ejer3 {
    public static void main(String[]args){
        String inputtext=JOptionPane.showInputDialog("cantidad en bolivianos");
        double cant=Double.parseDouble(inputtext);
        String money = JOptionPane.showInputDialog("a que moneda desea convertit");
        JOptionPane.showMessageDialog(null, convert (cant, money));
    }
    public static String convert(double cant, String money){
        double resp=0;
        switch (money){
            case "dolares":
                resp=cant*0.14451;
            break;
            case"euros":
                resp=cant*0.12938;
            break;
            case "libras":
                resp=cant*0.11552;
                break;
            default:
                JOptionPane.showMessageDialog(null, "por favor introduzca un convertidor aceptado: ");
                break;
        }
        DecimalFormat df=new DecimalFormat("#.00");
        return "la cantidad en: "+money+" es"+df.format(resp);
    }
}
